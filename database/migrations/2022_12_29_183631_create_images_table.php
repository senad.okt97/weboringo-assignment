<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->string('en');
            $table->string('sr');
            $table->timestamps();
        });

        // todo: this should be put as a seeder and not in migration
        $images = [
            [
                'url' => 'happy.jpeg',
                'en' => 'happy',
                'sr' => 'srećan'
            ],
            [
                'url' => 'nice.jpeg',
                'en' => 'nice',
                'sr' => 'lijepo'
            ],
            [
                'url' => 'friend.jpeg',
                'en' => 'friend',
                'sr' => 'prijatelj'
            ],
            [
                'url' => 'speed_limit.jpeg',
                'en' => 'speed limit',
                'sr' => 'ograničenje brzine'
            ],
            [
                'url' => 'explosion.jpeg',
                'en' => 'explosion',
                'sr' => 'eksplozija'
            ],
            [
                'url' => 'carrot.jpeg',
                'en' => 'carrot',
                'sr' => 'mrkva'
            ],
            [
                'url' => 'car.jpeg',
                'en' => 'car',
                'sr' => 'auto'
            ],
            [
                'url' => 'soldier.jpeg',
                'en' => 'soldier',
                'sr' => 'vojnik'
            ],
            [
                'url' => 'fear.jpeg',
                'en' => 'fear',
                'sr' => 'strah'
            ],
            [
                'url' => 'relativity.jpeg',
                'en' => 'relativity',
                'sr' => 'relativnost'
            ],
            [
                'url' => 'book.jpeg',
                'en' => 'book',
                'sr' => 'knjiga'
            ],
            [
                'url' => 'earth.jpeg',
                'en' => 'earth',
                'sr' => 'zemlja'
            ],

        ];

        foreach ($images as $image) {
            DB::table('images')->insert(
            array(
                'url' => $image['url'],
                'en' => $image['en'],
                'sr' => $image['sr']
            ));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
};