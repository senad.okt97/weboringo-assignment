<?php
 
namespace App\Http\Controllers;
 
use App\Models\Image;
 
class ImageController extends Controller
{
    /**
     * Returns images
     */
    protected $images;
    public function __construct(){
        $this->images = new Image();
    }
    public function get()
    {
    	$images = Image::all();
    	return $images->toJson();
    }
}